<?php

namespace Stamphpede\ServiceManager;

use League\Container\Container;
use Psr\Container\ContainerInterface;

class ContainerBuilder
{
    private array $modules;

    public function __construct(array $modules)
    {
        $this->modules = $modules;
    }

    public function buildContainer(): Container
    {
        $container = new Container();
        $container->add(ContainerInterface::class, $container);

        $containerConfigLoader = new ServiceLoader($container);

        foreach ($this->modules as $moduleClass) {
            $module = new $moduleClass;
            if (method_exists($module, 'getServices')) {
                $containerConfigLoader->loadServices($module->getServices());
            }
        }

        return $container;
    }
}
