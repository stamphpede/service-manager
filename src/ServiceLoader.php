<?php

namespace Stamphpede\ServiceManager;

use League\Container\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

class ServiceLoader
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function loadServices(array $serviceConfig)
    {
        foreach ($serviceConfig as $service => $config) {
            if (isset($config['class'])) {
                $definition = $this->container->add($service, $config['class']);
            } elseif (isset($config['factory'])) {
                $definition = $this->container->add($service, $config['factory']);
            } else {
                $definition = $this->container->add($service);
            }

            if (isset($config['args'])) {
                $definition->addArguments($config['args']);
            }

            $definition->setShared(true);
        }
    }
}
